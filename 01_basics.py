#!/usr/bin/python

# Printing a line
print(">>>> Hello, welcome to python-eamples")

# If-else statements
if True:
    print(">> Yes, it is...")
    print("True")
else:
    print(">> Yes, it is...")
    print("False")

check=1
if check == 1:
    print("Check is 1");
elif check == 2:
    print("Check is 2")
else:
    print("Check is rest")

# Quote
quote1 = 'Quote1'
quote2 = "Quote 2"
quote3 = """Quote 3 
    multiline"""
print(quote1);
print(quote2);
print(quote3);

# multi-line statement
test_multiline = "Hello " + \
    "multi " + \
    "line"
print(">>> " + test_multiline)

# test comment
'''
test multi line comment
'''

print(">>>> File end....")
