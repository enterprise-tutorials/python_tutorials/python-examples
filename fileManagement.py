import sys

# try-catch
input("\n\n Press enter key")

file_name = "dummyfile.txt"
try:
    # open a file
    file = open(file_name, "w")
except IOError:
    print("There was error writing to ", file_name)
    sys.exit()

