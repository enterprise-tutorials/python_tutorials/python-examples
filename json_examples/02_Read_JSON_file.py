# Example 2: Python read JSON file
# Deserialization

import json

# Case 1: Read from file
with open('person_read.json') as f:
    data = json.load(f)

# Output: {'name': 'Maggie', 'languages': ['English', 'Malayalam']}
print("data =>", data)

# Case 2: Read from String
json_string = """
{
    "researcher": {
        "name": "Maggie Angel",
        "species": "Woman",
        "relatives": [
            {
                "name": "Rickie Malt",
                "species": "Man"
            }
        ]
    }
}
"""
data = json.loads(json_string)
print("data =>", data)
