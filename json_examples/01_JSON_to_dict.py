# Example 1: Python JSON to dict

import json

person = '{"name": "Maggie", "languages": ["English", "Malayalam"]}'
person_dict = json.loads(person)

# Output: {'name': 'Maggie', 'languages': ['English', 'Malayalam']}
print("person_dict =>", person_dict)

# Output: ['English', 'Malayalam']
print("person_dict['languages'] =>", person_dict['languages'])
