# Example 3: Convert dict to JSON

import json

person_dict = {'name': 'Maggie',
               'age': 12,
               'children': None
}
person_json = json.dumps(person_dict)

# Output: {"name": "Maggie", "age": 12, "children": null}
print("person_json => ", person_json)
