# Example 5: Trying a real-world example
# Reads a JSON and looksup a value

import json

json_string = """
{
    "researcher": {
        "name": "Maggie Angel",
        "species": "Woman",
        "cars": [ "Ford", "BMW", "Fiat" ],
        "relatives": [
            {
                "name": "Rickie Malt",
                "species": "Man"
            }
        ]
    }
}
"""

if json_string is not None:
    data = json.loads(json_string)
    print("data =>", data)
    if 'cars' in data['researcher']:
        print("data['cars'] =>", data['researcher']['cars'])


json_str = """
{
    "ad":"test",
    "tags":["gello", "hello", "mello"]
}
"""
_print_str = None
json_obj = json.loads(json_str)
if json_obj is not None:
    print('JSON Object: %s' % json_obj)
    if 'tags' in json_obj:
        _print_str = json_obj.get('tags')
print('Extracted Tags: %s' % _print_str)
