# Example 4: Writing JSON to a file
# Serialization

import json

person_dict = {
    "name": "Maggie",
    "languages": ["English", "Malayalam"],
    "married": True,
    "age": 32
}

with open('person_write.json', 'w') as json_file:
    json.dump(person_dict, json_file)
