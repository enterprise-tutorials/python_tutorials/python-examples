# Example 5: Python pretty print JSON

import json

person_string = '{"name": "Maggie", "languages": "English", "numbers": [2, 1.6, null]}'

# Getting dictionary
person_dict = json.loads(person_string)

# Pretty Printing JSON string back
print("person_dict => ", json.dumps(person_dict, indent = 4, sort_keys=True))
