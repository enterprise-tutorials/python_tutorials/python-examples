#!/usr/bun/python3

import sys, getopt

def main (argv):
    inputfile = ''
    outputfile = ''

    print("Number of arguments: ", len(sys.argv), "arguments")
    print("Argument List: ", str(sys.argv))

    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('Usage: thisfile.py -i <inputfile> -o <outputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print("Usage: thisfile.py -i <inputfile> -o <outputfile>")
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    print("Input file is ", inputfile)
    print("Output file is ", outputfile)


if __name__ == "__main__":
    main(sys.argv[1:])