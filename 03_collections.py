#!/usr/bin/python

import sys

print(">>> --------- List --------- ")
listVar1 = ['abcd', 786, 2.23, 'john', 70.2]
listVar2 = ['new1', 'new2']
print(">> listVar1 : ", listVar1)
print(">> listVar1[0] : ", listVar1[0])
print(">> listVar1[0:2] : ", listVar1[0:2])
print(">> listVar1 + listVar2 : ", listVar1 + listVar2)
del(listVar1[2])
print(">> After item delete from listVar1 : ", listVar1)
print()

print(">>> --------- Tuples --------- ")
tupleVar1 = ('abcd', 40, 'xyz', 10.3, 88.98)
tupleVar2 = ('mnop', 'defr')
print(">> tupleVar1 : ", tupleVar1)
print(">> tupleVar1[0] : ", tupleVar1[0])
print(">> tupleVar1[:3] : ", tupleVar1[:3])
print(">> tupleVar1 + tupleVar2 : ", tupleVar1 + tupleVar2)
print()

print(">>> --------- Dictionary --------- ")
dictionaryVar1 = {}
dictionaryVar1['one'] = 'This is one'
dictionaryVar1[2] = "This is two"
dictionaryVar2 = {'name':'Kyle', 'code':1234}
print(">> dictionaryVar1 : ", dictionaryVar1)
print(">> dictionaryVar2 : ", dictionaryVar2)
print(">> str(dictionaryVar1) : " + str(dictionaryVar1))
print(">> repr(dictionaryVar1) : " + repr(dictionaryVar1))
print()

print(">>> --------- Something ----------")
print(">>> range1: ", range(5))
print(">>> range2: ", range(1,6,1))

i=0
value1 = {i for i in range(5)}
print(">> value1 : " , value1)

offset = None
offset = 0 if offset is None else offset
value2 = {i for i in range(offset,5,1)}
print(">> value2 : " , value2)
