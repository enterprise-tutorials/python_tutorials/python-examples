# python-examples
Examples tried out during my learning of Python
Used Python3

### Pre-requisites
* I used [pyenv](https://github.com/pyenv/pyenv) tool for the ease of setup.
There are many tutorials online and [here](https://realpython.com/intro-to-pyenv/) is one of them.
* I used [PyCharm](https://www.jetbrains.com/pycharm/) editor. This example has my env settings as well.

### Setup
* Virtual environment
```
pipenv install venv

make venv_dev
```

### Acknowledgements
* First and foremost, my God for life that He has given
* Secondly, my wife & children
* Thirdly, my relatives, friends, and supporters

### License
This project is under open source [License](LICENSE.md)
