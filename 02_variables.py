#!/usr/bin/python

print(">>> --------- Number & String --------- ")
numberInt = 10
numberFloat = 1000.0
stringVar = "StringName"
multiAssignment1 = multiAssignment2 = "MultiAsignment"
var1, var2, var3 = 1, 2, "var3"

print(">> numberInt : " + str(numberInt))
print(">> numberFloat : " + str(numberFloat))
print(">> stringVar : " + stringVar)
print(">>>> substring of " + stringVar + " : " + stringVar[2:5])
print(">> multiAssignment1 : " + multiAssignment1)
print(var1)
print(var2)
print(var3)

value = ""
value = int(value)
print(">> value : " + str(value))
if value and value < 0:
    print(">> value less than 0")
